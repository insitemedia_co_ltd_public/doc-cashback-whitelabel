---
sidebar_position: 1
---
# Introduction

![Example banner](/img/logo.svg)

Let's discover **Cashback White Label Program in less than 5 minutes**.

## myCashback is an end-to-end Enterprise Cashback Solution.

## What is Cashback?

Cashback is the newest trend in the e-commerce industry for customer acquisition, retention, better branding and efficient market capitalization.

## Why Cashback?

Research shows cashback is a powerful incentive for consumers to return, often within a week of receiving their cashback rewards, to spend more on their favorite online shopping sites.

## Market Size

Cashback market size is $ 500 billion.

## How Cashback effect your value?

Cashback promotions create a 3.4 times lift in conversion rate. An average order value increase of 46% (from $76 to $106) according to research.
