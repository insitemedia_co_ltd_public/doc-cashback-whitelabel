---
sidebar_position: 1
---
# Getting Started with myCashback whitelabel

The Cashback Whitelabel Platform is a comprehensive solution to empower cashback platform to quickly start ecommerce businesses. It not only helps you to save the hardware deployment costs but also provides you with operation and maintenance support. You can also enjoy the regular service updates, new product features and customer service.

