sidebar_position: 3

# Integration

## Learn how to integrate with myCashback platform

After confirmed the UI/UX design, publisher will need to prepare APIs for myCashback.

### Required APIs from publisher:

1. User Profile API
2. Notification API (in case that it's application version)
3. Disbursement API

#### User Profile API

The purpose of user profile API is for myCashback microsite to get user ID.

**Sample Request**

```
curl -X GET

'https://yourdomain.com/user/profile',\

'Authorization: Bearer b85b725f995e67bbb7fc4585fb1d947d  (your-access-token)
```

The token is generated from publisher side. The token will be added with our URL.

For instance, https://XXX.mycashback.io?token=b85b725f995e67bbb7fc4585fb1d947d&lang=en

**Sample Success Response**

```
{
   "status":{
      "code":"200",
      "message":"success"
   },
   "Data":{
      "user_id":"10001609786"
   }
}
```

Once we receive success response, we will be able to get user ID.

**Sample Error Response**

```
{
   "status":{
      "code":"401",
      "message" :  "unauthorized"
   },
}
```

If this token is incorrect, we will get message "unauthorized".

#### Notification API

The purpose of this API is to notify user when there is a new cashback transaction or when the transaction is approved or when user has withdraw the cashback successfully.

**Sample Resquest**

```
curl -X GET \

'https://yourdomain.com/notify', \

-H 'Authorization: Bearer b85b725f995e67bbb7fc4585fb1d947d  (your-access-token) \
--data-raw '{ "title": "you receive a new cash back", "message": "100 MMK" }'

```

**Sample Success Response**

```
{
   "status":{
      "code":"200",
      "message":"success"
   }
}

```

**Sample Error Response**

```
{
   "status":{
      "code":"401",
      "message" :  "unauthorized"
   },
}
```

#### Disbursement API

The purpose of this API is for the disbursement flow, where user withdraw the cashback to the wallet or bank account. The disbursement source can be choose from publisher side. In case user has own the wallet. Please send us the API to top up money to the user's wallet account.
