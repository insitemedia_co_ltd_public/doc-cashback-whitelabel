---
sidebar_position: 2
---
# Requirement

## **Publisher's requirement:**


| No. | Steps                                                                                                                                          |
| :---- | :----------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.  | Please prepare the UI guideline, in case that you would like to make the change to have the look and feel same as your application or website. |
| 2.  | Prepare the position of the myCashback icon on your application or website.                                                                    |
| 3.  | Implement the user profile API and notification API (application case).                                                                        |
| 4.  | Send us the API spec and postman if posible.                                                                                                   |
| 5.  | Decide which disbursement source(wallet,, you are using for disbursement flow.<br /><br />                                                     |
| 6.  | For the publisher that has own wallet, please send us the API spec and how to setup the account.                                               |

On shelf paument gateway that we have are:

* Paypal
* True Money (Thai customer)
* Direct Banking (Thai customer)


## **myCashback's provided:**

1. Redesign UI/UX of mycashback to fit the look and feel of publisher's application.
2. Subdomain for the microsite.

   * for example: https://XXX.mycashback.io
3. Web Portal: Back Office website where publisher can monitor.

   * [https://cws-dev.mycashback.io/](https://cws-dev.mycashback.io/)
4. In case publisher would like to get raw data, we also have APIs provided.

   * [https://documenter.getpostman.com/view/14529386/TzseJ6TZ](https://documenter.getpostman.com/view/14529386/TzseJ6TZ)
