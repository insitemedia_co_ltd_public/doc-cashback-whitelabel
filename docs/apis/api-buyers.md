---
sidebar_position: 6
---
# Buyers

## Document Model


| Variable Name     | Description                                                                         | Type               | Sample data                                                                                                                                                                                                                                                  | Remark                              |
| ------------------- | ------------------------------------------------------------------------------------- | -------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| id                | User ID                                                                             | ObjectId           | 5f4f754c1325b29fd9f9b700                                                                                                                                                                                                                                     |                                     |
| publisherId       | Publisher ID                                                                        | ObjectId           | 5dcfb758ba596f3a9dbb6700                                                                                                                                                                                                                                     |                                     |
| buyerId           | Buyer ID                                                                            | String             | 40dae875-6b4b-4b6b-9814-d0b13343f4a3                                                                                                                                                                                                                         |                                     |
| buyerToken        | Buyer's Token                                                                       | String             | eyJpdiI6Ikg1ZnY2dWN4U0Q0b25oNU5VdWx2T0E9PVxuIiwidmFsdWUiOiJoSk5pVVJvWDJ0UkFLNXFTU1l3RWtxT3hLanJGNmVTTXFVanFvbk5rd1kyMlNSSUV0cHAzSUFwMnVHcGJcbmFKOWlcbiIsIm1hYyI6IjgxMjI3ZTkwZTRhM2Y3ZDY0OWIxNjUwNmYxOGE1MTg5OGYzOTQwMDg3NzAyYTdlOTFjM2NiZGQ5MDQ5MzA0ODEifQ== |                                     |
| firstName         | Buyer's Firstname                                                                   | String             | John                                                                                                                                                                                                                                                         | Format is depend on each publisher. |
| lastName          | Buyer's Lastname                                                                    | String             | Doe                                                                                                                                                                                                                                                          |                                     |
| phoneNumber       | Buyer's phone number                                                                | String             | 0812345678                                                                                                                                                                                                                                                   |                                     |
| gender            | Buyer's Gender                                                                      | String (enumerate) | "M"                                                                                                                                                                                                                                                          | ["M", "F", "O"]                     |
| dateOfBirth       | Date of birth                                                                       | Date               | 2020-08-21T08:16:25.438Z                                                                                                                                                                                                                                     | ISO Format                          |
| email             | email address                                                                       | String             | john.doe@gmail.com                                                                                                                                                                                                                                           |                                     |
| facebookIdentity  | Facebook account                                                                    | String             | john.doe                                                                                                                                                                                                                                                     |                                     |
| instagramIdentity | Instagram account                                                                   | String             | john.doe                                                                                                                                                                                                                                                     |                                     |
| twitterIdentity   | Twitter account                                                                     | String             | john.doe                                                                                                                                                                                                                                                     |                                     |
| rating            | Rating percentage                                                                   | Number             | 80                                                                                                                                                                                                                                                           | 0 to 100                            |
| balance           | Wallet Balance                                                                      | [Objects]          | `[{"account": 11,"currency": "THB", "_id": "5f3f82d9f610360012c23ab1"}]`                                                                                                                                                                                     |                                     |
| binded            | Binding Status                                                                      | String             | true                                                                                                                                                                                                                                                         | [true, false]                       |
| banned            | Ban Status                                                                          | Boolean            | false                                                                                                                                                                                                                                                        | [true, false]                       |
| bannedNote        | The reason of why we banned this user                                               | Boolean            | "หลักฐานปลอม"                                                                                                                                                                                                                                     |                                     |
| address           | Buyer adress                                                                        | String             |                                                                                                                                                                                                                                                              |                                     |
| city              | Buyer's city                                                                        | String             |                                                                                                                                                                                                                                                              |                                     |
| metadata          | Other user information, such as the status of participation in promotion campaigns. | Object             | {"joinedStairSequenceBonus": false, "joinedVipBonus": false}                                                                                                                                                                                                 |                                     |
| referral          | Code for recommand to friends                                                       | String             |                                                                                                                                                                                                                                                              |                                     |
| createdAt         | Create Date                                                                         | Date               | 2020-08-21T08:16:25.438Z                                                                                                                                                                                                                                     | ISO Format                          |
| updatedAt         | Update Date                                                                         | Date               | 2020-08-21T08:16:25.438Z                                                                                                                                                                                                                                     | ISO Format                          |

## HTTP Request

```
GET https://cws.mycashback.io/api/buyers
```

## Hearder


| Header Name    | Description      | Remark                               |
| ---------------- | ------------------ | -------------------------------------- |
| Authentication | Bearer {{token}} | use the token that get from auth API |
| Publisher ID   | {{Publisher_id}} |                                      |

## Request Parameters


| Parameters Name | Description                            | Remark |
| :---------------- | :--------------------------------------- | -------- |
| limit           | limit the number of user that show     |        |
| skip            | skip the amount of user                |        |
| search          | search keyword                         |        |
| sort[_id]       | sort[field_name]  1  = asc , -1 = desc |        |

## Response Parameters


| Parameters Name | Description    | Remark                                               |
| ----------------- | :--------------- | ------------------------------------------------------ |
| success         | success status | status will be`true` or `false`                      |
| error           | error          | if success staus is`false` will show an error object |
| data            | object         | data of all the buyers                               |
