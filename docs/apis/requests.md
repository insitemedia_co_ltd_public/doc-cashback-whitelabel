---
sidebar_position: 2
---
# API Requests


| Purpose                       | Endpoint     | Remarks                                                  |
| :------------------------------ | -------------- | :--------------------------------------------------------- |
| Authenticating API key        | /auth        | You must have a valid "*token* " before making requests. |
| Get all product offers        | /products    | Getting a full list of product offers                    |
| Get all merchant offers       | /merchnats   | Getting a full list of merchant offers                   |
| Get all buyers                | /buyers      | Getting a full list of buyers                            |
| Get all cashback transactions | /cashbacks   | Getting a full list of cashback transactions             |
| Get all publisher commission  | /commissions | Getting a full list of commission                        |
| Download invoice              | /invoices    | Getting a invoice file                                   |
