---
sidebar_position: 3
---
# Authentication

The system requires authentication before use by working through an access token.

## API Key

You need to Authenticate your API key before being able to call any API request from mycashback. Once you already authenticate you will receive a "token" which will be expired in 2 hours.

## HTTP Request

```
POST https://cws.mycashback.io/api/authenticate
```

## Request Parameters


| Parameters Name | Description       | Remark |
| ----------------- | :------------------ | -------- |
| identity        | username or email |        |
| password        | login password    |        |

## Response Parameters


| Parameters Name | Description    | Remark                                                                                                                      |
| ----------------- | :--------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| success         | success status | status will be`true` or `false`                                                                                             |
| error           | error          | if success staus is`false` will show an error object                                                                        |
| token           | JWT Token      | This will be your token. It can be used only for 2 hours once your token expires you need to request for a new token again. |
