---
sidebar_position: 7
---
# Cashback Transactions

## Document Model


| Variable Name     | Description                                                           | Type               | Sample Data                                                                                                                | Remark                              |
| ------------------- | ----------------------------------------------------------------------- | -------------------- | ---------------------------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| _id               | myCashback User ID                                                    | ObjectId           | 5fd99545f5e3a1001208a50e                                                                                                   |                                     |
| publisherId       | publisher ID                                                          | ObjectId           | 5dcfb758ba596f3a9dbb6700                                                                                                   |                                     |
| buyerId           | user ID from publisher                                                | String             | 40dae875-6b4b-4b6b-9814-d0b13343f4a3                                                                                       | Format is depend on each publisher  |
| sku               | Product Code                                                          | String             |                                                                                                                            |                                     |
| conversionId      | Cashback ID                                                           | String             | SHP-82282604120425-3950376897-2                                                                                            | Format is depend on each merchant   |
| conversionDate    | Date and time the item was received                                   | Date               | 2020-08-21T08:16:25.438Z                                                                                                   | ISO Format                          |
| referUrl          | Referal URL                                                           | String             |                                                                                                                            |                                     |
| saleAmount        | Sale Amount                                                           | Number             | 222.13                                                                                                                     |                                     |
| cashbackAmount    | The amount of cashback that customer receives                         | Number             | 3.82                                                                                                                       |                                     |
| approvedAmount    | Total cashback amount                                                 | Number             | 5.45                                                                                                                       |                                     |
| bonus             | bonus                                                                 | Number             | 0                                                                                                                          |                                     |
| merchantName      | Store name                                                            | String             | JD Central                                                                                                                 |                                     |
| merchantLogo      | Merchant's logo                                                       | String             | [https://storage.googleapis.com/mcb-global/1614915125363.png](https://storage.googleapis.com/mcb-global/1614915125363.png) | Image URL Format                    |
| offerName         | Store reference name                                                  | String             | JD Central                                                                                                                 |                                     |
| affiliateNetwork  | Affiliate Network                                                     | String             | ACCESSTRADE                                                                                                                |                                     |
| status            | status                                                                | String (enumerate) | APPROVED                                                                                                                   | ["APPROVED", "PENDING", "REJECTED"] |
| orderId           | Order ID                                                              | String             |                                                                                                                            |                                     |
| category          | Product Category                                                      | String             |                                                                                                                            |                                     |
| productTitle      | Product Title                                                         | String             | Adapter                                                                                                                    |                                     |
| currency          | Currency                                                              | String             | THB                                                                                                                        |                                     |
| accepted          | Accept status                                                         | Boolean            | true                                                                                                                       | [true, false]                       |
| acceptedType      | Get a cashback by                                                     | String             | PROOF                                                                                                                      | PROOF, CREDITCARD, NONE             |
| paidBuyer         | Payment status                                                        | Boolean            | false                                                                                                                      | [true, false]                       |
| paidPublisher     | The lead status is the item that the publisher receives a commission. | Boolean            | false                                                                                                                      | [true, false]                       |
| madeCommission    | The lead status is the item that the publisher receives a commission. | Boolean            | false                                                                                                                      | [true, false]                       |
| note              | note                                                                  | String             |                                                                                                                            |                                     |
| usedRating        | Approved with rating                                                  | Boolean            | false                                                                                                                      | [true, false]                       |
| merchantApproved  | Approved by merchant                                                  | Boolean            | false                                                                                                                      | [true, false]                       |
| rejectReason      | Reason of rejected                                                    | String             |                                                                                                                            |                                     |
| proofReason       | Reason of prove                                                       | String             |                                                                                                                            |                                     |
| orderProofFile    | URL of proof file                                                     | String             |                                                                                                                            |                                     |
| orderProofFiles   | URL of proof files                                                    | [Objects]          |                                                                                                                            |                                     |
| orderProofStatus  | Status of proof                                                       | String             |                                                                                                                            |                                     |
| orderProofDate    | Date of sent proof                                                    | Date               |                                                                                                                            |                                     |
| proofBy           | Proof                                                                 | String             |                                                                                                                            |                                     |
| proofDate         | Approved Date                                                         | Date               | 2020-08-21T08:16:25.438Z                                                                                                   | ISO Format                          |
| archived          | Hide transaction                                                      | Boolean            | false                                                                                                                      | [true, false]                       |
| hasSequenceBonus  | Sequence Bonus Status                                                 | Boolean            | false                                                                                                                      | [true, false]                       |
| hasVipBonus       | VIP Status                                                            | Boolean            | false                                                                                                                      | [true, false]                       |
| addToAIProofQueue | AI Approval                                                           | Boolean            | false                                                                                                                      | [true, false]                       |
| createdAt         | Created Date                                                          | Date               | 2020-08-21T08:16:25.438Z                                                                                                   | ISO Format                          |
| updatedAt         | Updated Date                                                          | Date               | 2020-08-21T08:16:25.438Z                                                                                                   | ISO Format                          |


## HTTP Request

```
GET https://cws.mycashback.io/api/transactions
```

## Hearder


| Header Name    | Description      | Remark                               |
| ---------------- | ------------------ | -------------------------------------- |
| Authentication | Bearer {{token}} | use the token that get from auth API |
| Publisher ID   | {{Publisher_id}} |                                      |

## Request Parameters


| Parameters Name | Description                            | Remark |
| :---------------- | :--------------------------------------- | -------- |
| limit           | limit the number of user that show     |        |
| skip            | skip the amount of user                |        |
| search          | search keyword                         |        |
| sort[_id]       | sort[field_name]  1  = asc , -1 = desc |        |

## Response Parameters


| Parameters Name | Description    | Remark                                               |
| ----------------- | :--------------- | ------------------------------------------------------ |
| success         | success status | status will be`true` or `false`                      |
| error           | error          | if success staus is`false` will show an error object |
| data            | object         | data of all the buyers                               |
