---
sidebar_position: 1
---
# Introduction

The Publisher API offers a set of API calls that allow you to pull data about the advertiser program on the MCB platform, the commission you can earn from those advertiser programs, your transactions and campaign details.

## Limitation / Throttling

To guarantee smooth operation for all our publishers, we currently have a throttling in place that limits the number of API requests to 20 API calls per minute per account.

## Standard Pagination

Certain endpoints will generate large result sets. To save bandwidth and long processing times, these endpoints will paginate the results to reduce the strain on both client and server.

Any endpoint that supports pagination will return 3 attributes to support this:

* "offset" the offset of the data returned
* "limit" the number of results to which the output was restricted to 100 per request
* "total" the total number of results available
