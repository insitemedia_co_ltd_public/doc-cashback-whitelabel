const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Cashback White Label',
  tagline: 'end to end enterprise cashback solutions',
  url: 'https://mycashback.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Insite Media Co,. Ltd.',
  projectName: 'Cashback White Label',
  themeConfig: {
    navbar: {
      title: 'Cashback White Label',
      logo: {
        alt: 'Cashback White Label',
        src: 'img/logo.svg',
      },
      items: [
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Tutorial',
        },
        { to: '/docs/apis/introduction', label: 'APIs', position: 'left' },
        {
          type: 'docsVersionDropdown',
        },
        {
          type: 'localeDropdown',
          position: 'right',
        },
        // { to: '/api-docs', label: 'API Docs', position: 'left' },
        // {
        //   href: 'https://github.com/facebook/docusaurus',
        //   label: 'GitHub',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Get Started',
              to: '/docs/intro',
            },
            {
              label: 'API Docs',
              to: '/docs/apis/introduction',
            },
          ],
        },
        {
          title: 'Other Products',
          items: [
            {
              label: 'MCB Account',
              href: 'https://account.mycashback.io',
            },
            {
              label: 'Cashback Button',
              href: 'https://cbb.mycashback.io',
            },
            {
              label: 'Ads Platform',
              href: 'https://adms.mycashback.io',
            },
          ],
        },
        {
          title: 'Contact',
          items: [
            {
              label: 'Email',
              to: 'mailto:info@mycashback.co',
            },
            {
              label: 'Sales',
              href: 'mailto:tay@mycashback.co',
            },
            {
              label: 'Address',
              href: 'https://goo.gl/maps/iAzi6ywyz14XK9oRA',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Cashback White Label`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        // docs: {
        //   sidebarPath: require.resolve('./sidebars.js'),
        //   // Please change this to your repo.
        //   editUrl:
        //     'https://github.com/facebook/docusaurus/edit/master/website/',
        // },
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   editUrl:
        //     'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'th'],
  },
};
