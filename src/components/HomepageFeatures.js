import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'We pay in 1 day!',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        1st in the world with 1 day cashback.
      </>
    ),
  },
  {
    title: 'Price comparison',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        1st in the world to have
        price comparison
        net of cashback
      </>
    ),
  },
  {
    title: 'Personalize',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        1st in the world to offer 20 million SKU proprietary A.I search engine using big data
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
